
var ischool =  {} ;  //namespace

/* 熱圖資料解析器 */
ischool.HeatDataParser = function(study_records) {

	var srs = study_records ;
	var wrapSRS ;	//把每個物件以 Record 物件包起來
	var sections = [];	//只要有暫停的跡象，就是分段的地方。
	var rows = [];	//只要是往後播放的，包括快轉，都在同一個 Row。只要有回撥，或間格超過 10 分鐘，就是另一個 Row，僅是為了畫面簡潔而設立的概念，但 Bill 不想採用。

	var currentIndex = 0;
	var currentSection ;
	var parseSections = function() {
		var targetRecord = srs[currentIndex];
		if (!targetRecord) {
			return ;
		}

		if (isNewSection(currentIndex)) {
			currentSection = ischool.createSection();
			sections.push(currentSection);
		}
		currentSection.addRecord(wrapSRS[currentIndex]);

		currentIndex += 1;
		parseSections();	//recursive for each study_record
	}

	//判斷是否是新的分段開始
	var isNewSection = function(index) {
		//1. 第一筆記錄當然是新分段啦
		if (index < 1) {
			return true ;
		}

		var prevRec = wrapSRS[index - 1];
		var currRec = wrapSRS[index];
		var nextRec = wrapSRS[index + 1];

		//2. 如果有一筆是開始的記錄，則開始分段。
		if (currRec.isStartPlay) {
			return true ;
		}

		//2.5 如果 duration > 0 ，但 小於0.01，
		//    這種記錄通常是看到影片結束時 pause 事件之後所觸發的 ended 事件所送出的。
		//    弔詭的狀況是，即使用戶端 pause 的紀錄先送出，ended 的紀錄緊接在後，
		//    但是網路傳送過程很可能發生 ended 紀錄比 pause 紀錄先寫入 DB的狀況，
		//    結果判斷 section 時會因為少了 pause 紀錄，而導致認為 ended 紀錄是另一個分段。
		//    這種紀錄，一定不會是分段。
		/*
			//例如：系統 07:24:16.459，影片 67.036 秒，播放時間僅 0.006 秒。通常這麼短的只有在最後 ended事件觸發的，而在這之前觸發的 pause 事件會紀錄實際的觀看秒數，但因這兩個事件的 request 幾乎同時送出，所以有時候 ended 事件紀錄反而會先寫入，所以這種 ended 的事件需要檢查前一筆或後一筆，系統時間差小於 0.1 秒的紀錄，就是 ended 事件之前的 pause 事件的紀錄。
		   {  
		      "uqid":"8a371d5d5c29",
		      "last_second_watched":"67.036",
		      "seconds_watched":"0.006",
		      "time_watched":"2015-10-01T07:24:16.459Z"
		   },
		   {  
		      "uqid":"8a371d5d5c29",
		      "last_second_watched":"66.584",
		      "seconds_watched":"14.436",
		      "time_watched":"2015-10-01T07:24:16.486Z"
		   },
		*/
		if ((currRec.duration > 0) && (currRec.duration < 0.01)) {
			return false;
		}

		//3. 如果與前一筆記錄的系統時間差，大於這筆記錄的觀看 duration，
		//   代表這筆也是暫停狀況發生後的第一筆，要分段。
		//   (通常暫停後開始的第一筆會是 00:00，但某些狀況可能沒有這筆，那麼就套用此判斷規)
		if ((currRec.systemEndTime.getTime() - prevRec.systemEndTime.getTime()) > (currRec.duration + 1) * 1000) {
			return true;
		}
		
		//4. 若目前記錄currRec的影片開始時間小於前一個影片的結束時間，
		//   表示有 rewind 的現象，也是分段開始。
		if (currRec.videoStartTime < prevRec.videoEndtime ) {
			return true;
		}

		//5. 若目前記錄currRec的影片結束時間與前一個影片的結束時間的差值，
		//   大於表示有 rewind 的現象，也是分段
		if ((currRec.videoEndtime - prevRec.videoEndtime) > (currRec.duration + 1) * 1000 ) {
			return true;
		}

		//5. 若目前記錄currRec的影片結束時間與前一個影片的結束時間的差值，
		//   大於表示有 rewind 的現象，也是分段
		if ((currRec.videoEndtime - prevRec.videoEndtime) > (currRec.duration + 1) * 1000 ) {
			return true;
		}

		return false ;
	}

	var wrapWithRecordObjet = function(records) {
		var result = [];
		$(records).each(function(index, rec) {
			result.push(ischool.createRecord(rec));
		})
		return result ;
	}

	wrapSRS = wrapWithRecordObjet(study_records);

	parseSections();

	return {
		getRows : function() {

		},

		getSections : function() {
			return sections;
		},
	}
}

/* 
    建立 Section 物件 
    Section 物件代表圖上一段連續的學習過程，
    當 pause 事件發生時才會有新的 Section。
*/
ischool.createSection = function() {
	var _wrapSRS = [];
	var _videoStartTime  ;
	var _videoEndTime  ;
	var _systemStartTime  ;
	var _systemEndTime  ;
		
	return {
		addRecord : function(wrapRecord) {
			_wrapSRS.push(wrapRecord);

			if (!_videoStartTime) {
				_videoStartTime = wrapRecord.videoStartTime ;
			}
			else if (wrapRecord.videoStartTime < _videoStartTime) {
				_videoStartTime = wrapRecord.videoStartTime ;
			}

			if (!_videoEndTime) {
				_videoEndTime = wrapRecord.videoEndTime ;
			}
			else if (wrapRecord.videoEndTime > _videoEndTime) {
				_videoEndTime = wrapRecord.videoEndTime ;
			}


			if (!_systemStartTime) {
				_systemStartTime = wrapRecord.systemStartTime ;
			}
			else if (wrapRecord.systemStartTime.getTime() < _systemStartTime.getTime()) {
				_systemStartTime = wrapRecord.systemStartTime ;
			}


			if (!_systemEndTime) {
				_systemEndTime = wrapRecord.systemEndTime ;
			}
			else if (wrapRecord.systemEndTime.getTime() > _systemEndTime.getTime()) {
				_systemEndTime = wrapRecord.systemEndTime ;
			}
		},
		getVideoEndTime : function() {
			return _videoEndTime;
		}
		,
		getVideoStartTime : function() {
			return _videoStartTime;
		}
		,
		getSystemStartTime : function() {
			return _systemStartTime;
		}
		,
		getSystemEndTime : function() {
			return _systemEndTime;
		}
		,
		getDuration: function() {
			return (this.getVideoEndTime() - this.getVideoStartTime());
		},
		wrapRecords : _wrapSRS
	}
}

/*
	Record 物件包裝一筆 Study History Record
*/
ischool.createRecord = function(rec) {
	var _rec = rec ;
	var _isStartPlay = (_rec.seconds_watched == "0.0");
	var _duration = parseFloat(_rec.seconds_watched);

	var _videoEndTime = parseFloat(_rec.last_second_watched);
	var _videoStartTime = _videoEndTime - _duration ;
	if (_videoStartTime < 0) {
		_videoStartTime = 0;
	}

	var _systemEndTime = new Date(_rec.time_watched);
	var _systemStartTime = new Date(_systemEndTime.getTime() - _duration * 1000);


	return {
		isStartPlay :  _isStartPlay
		,
		videoEndTime : _videoEndTime
		,
		videoStartTime : _videoStartTime
		,
		duration : _duration
		,
		systemStartTime : _systemStartTime
		,
		systemEndTime : _systemEndTime
		,
		rawRecord : _rec 
	}
}
























